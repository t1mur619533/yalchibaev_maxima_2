namespace Class8;

public enum Color : int
{
    Black,
    Yellow,
    Red,
    White,
    Green
}