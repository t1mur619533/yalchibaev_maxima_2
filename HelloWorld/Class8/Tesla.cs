namespace Class8;

public class Tesla : Car
{
    public int Battery { get; set; }

    public Tesla(string name, Color color, int battery) : base(name, color)
    {
        Battery = battery;
        HasElectroEngine = true;
    }
    
    public Tesla(string name, Color color) : base(name, color)
    {
        HasElectroEngine = true;
    }

    public override void Print()
    {
        Console.ForegroundColor = ConsoleColor.Red;
        base.Print();
    }

    public new void SetColor()
    {
        Color = Color.Red;
    }
}