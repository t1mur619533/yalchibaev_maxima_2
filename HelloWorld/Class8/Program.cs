﻿// See https://aka.ms/new-console-template for more information

using Class8;

Car[] cars = new Car[3];
cars[0] = new Car("Nissan", Color.Black);
cars[1] = new Car("Mazda", Color.White);
cars[2] = new Tesla("Tesla Model S", Color.Red, 10_000);

for (int i = 0; i < cars.Length; i++)
{
    cars[i].Print();
}

var carClass = new Car("Niva", Color.Green);
var carClass2 = carClass;
carClass2.Name = "Patriot";
carClass.Print();

var carStruct = new CarStruct();
var carStruct2 = carStruct;
carStruct2.Color = Color.Red;
carStruct.Print();