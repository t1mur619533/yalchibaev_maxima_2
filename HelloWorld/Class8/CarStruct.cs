namespace Class8;

public struct CarStruct
{
    private string _name = "None";
    
    public Color Color { get; set; }

    public bool HasElectroEngine { get; set; }

    public string Name
    {
        get => _name;
        set => _name = value;
    }

    public CarStruct()
    {
        _name = "None";
        Color = Color.Black;
        HasElectroEngine = false;
    }
    
    public void Print()
    {
        Console.WriteLine($"Car: {Name}, Color: {Color}");
    }
}