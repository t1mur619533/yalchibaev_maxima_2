namespace Class8;

public class Car
{
    private string _name = "None";
    
    public virtual Color Color { get; protected set; }

    public bool HasElectroEngine { get; set; }

    public string Name
    {
        get => _name;
        set => _name = value;
    }

    public Car(string name, Color color)
    {
        _name = name;
        Color = color;
    }

    public virtual void Print()
    {
        Console.WriteLine($"Car: {Name}, Color: {Color}");
    }

    public void SetColor()
    {
        Color = Color.Black;
    }

    public void SetColor(Color color)
    {
        Color = color;
    }

    public void SetColor(int color)
    {
        Color = (Color) color;
    }
}