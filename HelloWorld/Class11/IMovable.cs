namespace Class11;

interface IMovable
{
    void Move();
}