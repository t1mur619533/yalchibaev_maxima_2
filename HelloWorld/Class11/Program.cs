﻿int i = 0;
while (i <= 0)
{
    try
    {
        Console.WriteLine("Введите положительное число!");
        var input = Console.ReadLine();
        try
        {
            var result = int.TryParse(input, out i);
            if (result)
            {
                Console.WriteLine($"Ваше число {i}");
            }
            else
            {
                Console.WriteLine("Не получилось спарсить!");
            }
        }
        catch (FormatException ex)
        {
            Console.WriteLine(ex.StackTrace);
            throw;
        }
        catch (OverflowException ex)
        {
            Console.WriteLine(ex.Message);
            throw;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
        
        // if (i <= 0)
        // {
        //     throw new MyCustomException(777, "Вы ввели отрицательное число или 0");
        // }

        // Console.WriteLine($"Ваше число {i}");
    }
    catch (MyCustomException ex)
    {
        Console.WriteLine($"Ошибка: {ex.Title}, Код: {ex.Code}");
    }
    catch (Exception ex)
    {
        Console.WriteLine("Что то пошло не так, попробуйте снова!");
    }
    finally
    {
        Console.WriteLine("");
    }
}

/*IMovable[] movables = new IMovable[3];
movables[0] = new Car();
movables[1] = new Moto();
movables[2] = new Car();

Car car = new Car();

IMovable movable = car;
movable.Move();

IMove move = car;
move.Move();

// for (int i = 0; i < movables.Length; i++)
// {
//     movables[i].Move();
// }*/