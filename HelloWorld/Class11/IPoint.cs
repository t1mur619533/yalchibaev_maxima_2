namespace Class11;

interface IPoint<T> where T : struct
{
    T X { get; }
    T Y { get; }
}