namespace Class11;

public class Car : INamed, IMovable, IMove, IPoint<int>
{
    public string Name => "Машина";

    void IMovable.Move()
    {
        Console.WriteLine("IMovable - car move");
    }
    
    void IMove.Move()
    {
        Console.WriteLine("IMove - car move");
    }

    public int X { get; }
    public int Y { get; }
}