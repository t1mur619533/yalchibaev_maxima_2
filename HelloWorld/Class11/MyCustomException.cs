public class MyCustomException : Exception
{
    public int Code { get;}
    public string Title { get;}

    public MyCustomException(int code, string title)
    {
        Code = code;
        Title = title;
    }
}