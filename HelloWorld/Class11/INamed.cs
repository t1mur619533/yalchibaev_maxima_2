namespace Class11;

interface INamed
{
    string Name { get; }
}