﻿using Class16;

List<Student> students = new List<Student>();
students.Add(new Student() { Id = 0, Age = 19, FirstName = "Ivan", LastName = "Ivanov" });
students.Add(new Student() { Id = 1, Age = 19, FirstName = "Alex", LastName = "Alexandrov" });
students.Add(new Student() { Id = 2, Age = 34, FirstName = "Sergey", LastName = "Sidorov" });
students.Add(new Student(){Id = 3, Age = 24, FirstName = "Denis", LastName = "Petrov"});
students.Add(new Student(){Id = 4, Age = 21, FirstName = "Timur", LastName = "Ivanov"});
students.Add(new Student(){Id = 5, Age = 16, FirstName = "Airat", LastName = "Ivanov"});

var query =
    from student in students
    where student.Age > 18 
    where student.Id > 3
    select new
    {
        Name = student.FirstName,
        Age = student.Age
    };

var method = students
    .Where(student => student.Age > 18)
    .Where(student => student.Id > 3)
    .Select(student => new {
        Name = student.FirstName,
        Age = student.Age
    });
    
List<Student> students2 = new List<Student>();
students2.Add(new Student() { Id = 0, Age = 56, FirstName = "Ivan", LastName = "Ivanov" });
students2.Add(new Student() { Id = 1, Age = 19, FirstName = "Alex", LastName = "Alexandrov" });
students2.Add(new Student() { Id = 2, Age = 18, FirstName = "Sergey", LastName = "Sidorov" });

var whereResult = students2
    .Where(student => student.Age > 18)
    .Distinct();

foreach (var student in whereResult)
{
    Console.WriteLine($"{student.FirstName} {student.LastName}");
}

Console.WriteLine("==============");

students2.Add(new Student(){Id = 3, Age = 24, FirstName = "Denis", LastName = "Petrov"});

foreach (var student in whereResult)
{
    Console.WriteLine($"{student.FirstName} {student.LastName}");
}

Console.WriteLine("==============GroupBy");
var groupBy = students.GroupBy(student => student.LastName);
foreach (var group in groupBy)
{
    Console.WriteLine(group.Key);
    foreach (var student in group)
    {
        Console.WriteLine($"{student.FirstName} {student.LastName}");
    }
}

Console.WriteLine("==============SelectMany");
var selectMany = groupBy.SelectMany(grouping => grouping).ToList();
foreach (var student in selectMany)
{
    Console.WriteLine($"{student.FirstName} {student.LastName}");
}

Console.WriteLine("==============Aggregate");
List<int> ints = new List<int> {1, 9, 2};
var aggregate = ints.Aggregate((i, i1) => i + i1);
var aggregate2 = ints.Aggregate((i, i1) => i - i1);
Console.WriteLine($"Aggregate : {aggregate}");
Console.WriteLine($"Aggregate : {aggregate2}");
List<string> strings = new List<string> {"one_", "two_", "next_"};
Console.WriteLine($"Aggregate : {strings.Aggregate((s, s1) => s + s1)}");

Console.WriteLine("==============Distinct");
List<int> int2 = new List<int> {1, 9, 3, 3, 4, 4, 4, 4, 4};
var distinct = int2.Distinct();
foreach (var i in distinct)
{
    Console.WriteLine(i);
}

Console.WriteLine("==============Except");
var except = ints.Except(int2);
foreach (var i in except)
{
    Console.WriteLine(i);
}

Console.WriteLine("==============Union");
var union = ints.Union(int2);
foreach (var i in union)
{
    Console.WriteLine(i);
}

Console.WriteLine("==============Concat");
var concat = ints.Concat(int2);
foreach (var i in concat)
{
    Console.WriteLine(i);
}

Console.WriteLine("==============Intersect");
var intersect = ints.Intersect(int2);
foreach (var i in intersect)
{
    Console.WriteLine(i);
}


Console.WriteLine("==============First");
try
{
    var first = ints.First(i => i > 10);
    Console.WriteLine(first);
}
catch (Exception e)
{
    Console.WriteLine(e);
}
Console.WriteLine("==============FirstOrDefault");
var firstOrDefault = ints.FirstOrDefault(i => i > 10);
Console.WriteLine(firstOrDefault);

Console.WriteLine("==============Last");
try
{
    var last = ints.Last();
    Console.WriteLine(last);
}
catch (Exception e)
{
    Console.WriteLine(e);
}
Console.WriteLine("==============LastOrDefault");
var lastOrDefault = students.LastOrDefault(student => student.Age > 18);
Console.WriteLine(lastOrDefault.FirstName);

Console.WriteLine("==============Single");
try
{
    var single = students.Single(student => student.Age == 16);
    Console.WriteLine(single.FirstName);
}
catch (Exception e)
{
    Console.WriteLine(e);
}
Console.WriteLine("==============SingleOrDefault");
try
{
    var singleOrDefault = students.SingleOrDefault(student => student.Age > 100);
    Console.WriteLine(singleOrDefault);
}
catch (Exception e)
{
    Console.WriteLine(e);
}

Console.WriteLine("==============ElementAt");
var elementAt = students.ElementAt(0);
Console.WriteLine(elementAt.FirstName);

Console.WriteLine("==============ElementAtOrDefault");
var elementAtOrDefault = ints.ElementAtOrDefault(54664);
Console.WriteLine(elementAtOrDefault);
