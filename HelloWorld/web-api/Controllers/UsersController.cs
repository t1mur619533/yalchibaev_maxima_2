using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using web_api.Models;
using web_api.Services;

namespace web_api.Controllers;

[Authorize]
[ApiController]
[Route("api/[controller]")]
public class UsersController : ControllerBase
{
    private readonly IUserService _userService;

    public UsersController(IUserService userService)
    {
        _userService = userService;
    }
    
    [Authorize(Roles = "Admin")]
    [HttpPost]
    public ActionResult Create(CreateUserDto userDto)
    {
        _userService.Create(userDto);
        return Ok();
    }

    [Authorize(Roles = "Admin,User")]
    [HttpGet("{id}")]
    public ActionResult Get(int id)
    {
        var userDto = _userService.Get(id);
        if (userDto != null)
        {
            return Ok(userDto);
        }
        return NotFound();
    }
    
    [Authorize(Policy = Policies.OnlyAdultPolicy)]
    [HttpGet]
    public async Task<ActionResult> GetAll()
    {
        var userDto = await _userService.Get();
        return Ok(userDto);
    }

    [HttpPut("{id}")]
    public ActionResult Update(int id, UserDto userDto)
    {
        var user = _userService.Get(id);
        if (user == null) 
            return NotFound();
        
        userDto.Id = id;
        _userService.Update(userDto);
        return Ok();
    }
}