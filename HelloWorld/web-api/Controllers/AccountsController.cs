using System;
using System.Threading.Tasks;
using Domain.Accounts.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace web_api.Controllers;

[Authorize]
[ApiController]
[Route("api/[controller]")]
public class AccountsController : ControllerBase
{
    private readonly IMediator _mediator;
    private readonly ILogger<AccountsController> _logger;

    public AccountsController(IMediator mediator, ILogger<AccountsController> logger)
    {
        _mediator = mediator;
        _logger = logger;
    }

    [AllowAnonymous]
    [HttpGet("/login")]
    public async Task<ActionResult<string>> Login([FromQuery] LoginQuery loginQuery)
    {
        try
        {
            _logger.LogInformation("Try login");
            var jwtToken = await _mediator.Send(loginQuery);
            return Ok(jwtToken);
        }
        catch (Exception exception)
        {
            _logger.LogError(exception.Message);
            return BadRequest();
        }
    }
    
    [HttpGet("/me")]
    public ActionResult<string> GetMyName()
    {
        return HttpContext.User.Identity.Name;
    }
}