using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Tickets;
using Domain.Tickets.Commands;
using Domain.Tickets.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace web_api.Controllers;

[Authorize]
[ApiController]
[Route("api/[controller]")]
public class TicketsController : ControllerBase
{
    private readonly IMediator _mediator;

    public TicketsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    /// <summary>
    /// Получение билетов
    /// </summary>
    /// <param name="getTicketsQuery"> пагинация </param>
    /// <returns> Результирующий набор билетов </returns>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(typeof(IEnumerable<TicketDto>), StatusCodes.Status200OK, "application/json")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult<IEnumerable<TicketDto>>> Get([FromQuery] GetTicketsQuery getTicketsQuery)
    {
        var ticketDtos = await _mediator.Send(getTicketsQuery);
        return Ok(ticketDtos);
    }
    
    /// <summary>
    /// Получение билета по идентификатору
    /// </summary>
    /// <param name="id"> идентификатор билета</param>
    /// <returns> Билет </returns>
    /// <response code="404">Билет с таким идентификатором не найден</response>
    [HttpGet("{id}")]
    [Produces("application/json")]
    [ProducesResponseType(typeof(TicketDto), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult> Get(int id)
    {
        var getTicketQuery = new GetTicketQuery { Id = id };
        var ticketDto = await _mediator.Send(getTicketQuery);
        return Ok(ticketDto);
    }

    [HttpDelete("{id}")]
    public void Delete([FromRoute] int id)
    {
        throw new NotImplementedException();
    }

    [HttpPost]
    public async Task<ActionResult> Create([FromBody] CreateTicketCommand createTicketCommand)
    {
        await _mediator.Send(createTicketCommand);
        return Ok();
    }
}