using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace web_api.Middlewares;

public class ApiKeyMiddleware : IMiddleware
{
    private readonly IApiKeyProvider _apiKeyProvider;

    public ApiKeyMiddleware(IApiKeyProvider apiKeyProvider)
    {
        _apiKeyProvider = apiKeyProvider;
    }

    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        if (context.Request.Headers["apikey"] == _apiKeyProvider.GetKey())
        {
            await next.Invoke(context);
        }
        else
        {
            context.Response.StatusCode = StatusCodes.Status403Forbidden;
        }
    }
}