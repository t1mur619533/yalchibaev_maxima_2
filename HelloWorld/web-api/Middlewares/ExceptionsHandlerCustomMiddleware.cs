using System;
using System.Net.Http.Json;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Domain.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace web_api.Middlewares;

public class ExceptionsHandlerCustomMiddleware
{
    private RequestDelegate _next;
    private ILogger<ExceptionsHandlerCustomMiddleware> _logger;


    public ExceptionsHandlerCustomMiddleware(RequestDelegate next, ILogger<ExceptionsHandlerCustomMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            await _next.Invoke(context);
        }
        catch (NotFoundException notFoundException)
        {
            _logger.LogError(notFoundException.Message);
            context.Response.StatusCode = StatusCodes.Status404NotFound;
            context.Response.ContentType = "application/json";
            var response = JsonSerializer.Serialize(new { errors = notFoundException.Message });
            await context.Response.WriteAsync(response);
        }
        catch (Exception exception)
        {
            _logger.LogError(exception.Message);
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            await context.Response.WriteAsync(exception.Message);
        }
    }
}