using System;
using System.IO;
using System.Text;
using DataAccessLayer;
using Domain;
using Domain.Security;
using Domain.Tickets.Commands;
using FluentValidation;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Serilog;
using web_api;
using web_api.Extensions;
using web_api.Middlewares;
using web_api.Models;
using web_api.Requirements;
using web_api.Services;

var builder = WebApplication.CreateBuilder(args);
builder.Host.UseSerilog((context, configuration) =>
{
    configuration.MinimumLevel.Debug();
    configuration.WriteTo.Console();
    configuration.WriteTo.File("log.txt");
});
var applicationOptions = builder.Configuration.Get<ApplicationOptions>();
builder.Services.Configure<DomainOptions>(builder.Configuration); //добавление конфигурации в контейнер
builder.Services.Configure<ApplicationOptions>(builder.Configuration); //добавление конфигурации в контейнер
// Add services to the container.
builder.Services.AddDbContext<DataContext>(optionsBuilder => optionsBuilder.UseLazyLoadingProxies().UseNpgsql(applicationOptions.DbConnection));
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    {
        options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
        {
            In = ParameterLocation.Header,
            Description = "Please insert JWT with Bearer into field",
            Name = "Authorization",
            Type = SecuritySchemeType.ApiKey,
            BearerFormat = "JWT"
        });
        
        options.AddSecurityRequirement(new OpenApiSecurityRequirement()
        {
            {
                new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference {Type = ReferenceType.SecurityScheme, Id = "Bearer"}
                },
                new string[] { }
            }
        });
        
        options.CustomSchemaIds(y => y.FullName);
    }
    {
        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "web-api.xml"));
    }
    {
        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Domain.xml"));
    }
});
builder.Services.AddTransient<IUserService, UserService>();
builder.Services.AddTransient<IApiKeyProvider, ApiKeyProvider>();
builder.Services.AddTransient<IJWTProvider, JWTProvider>();
builder.Services.AddTransient<ApiKeyMiddleware>();
builder.Services.AddTransient<AbstractValidator<UserDto>, UserValidator>();
builder.Services.AddFluentValidationAutoValidation();
builder.Services.AddValidatorsFromAssemblyContaining<UserValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<CreateTicketCommandValidator>();
builder.Services.AddAutoMapper(typeof(UserMapConfig),typeof(CreateTicketMapProfile));
builder.Services.AddMediatR(typeof(CreateTicketCommand));
builder.Services.AddScoped<IAuthorizationHandler, MinimalAgeRequirementHandler>();
builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    options.RequireAuthenticatedSignIn = false;
}).AddJwtBearer(options =>
{
    options.RequireHttpsMetadata = false;
    options.SaveToken = true;
    options.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(applicationOptions.Secret)),
        ValidateIssuer = false,
        ValidateAudience = false
    };
});
builder.Services.AddAuthorization(options => options.AddPolicy(Policies.OnlyAdultPolicy, policyBuilder => policyBuilder.AddRequirements(new MinimalAgeRequirement(18))));

var app = builder.Build();

app.UseCustomExceptionHandler();

// // Configure the HTTP request pipeline.
// if (app.Environment.IsDevelopment())
// {
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseReDoc(options => options.RoutePrefix = "redoc");
// }

app.UseHttpsRedirection();

app.UseRouting();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();