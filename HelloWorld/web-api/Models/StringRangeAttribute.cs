using System.ComponentModel.DataAnnotations;
using Domain.Tickets;

namespace web_api.Models;

public class TicketNameLengthAttribute : ValidationAttribute
{
    private readonly int _from;
    private readonly int _to;

    public TicketNameLengthAttribute(int from, int to)
    {
        _from = from;
        _to = to;
    }

    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        var ticket = (TicketDto) validationContext.ObjectInstance;
        if (ticket.Name.Length < _from || ticket.Name.Length > _to)
        {
            return new ValidationResult($"Длина должна быть от {_from} до {_to}");
        }
        return ValidationResult.Success;
    }
}