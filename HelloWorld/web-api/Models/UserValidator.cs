using FluentValidation;

namespace web_api.Models;

public class UserValidator : AbstractValidator<UserDto>
{
    public UserValidator()
    {
        RuleFor(user => user.Id).NotEmpty().GreaterThanOrEqualTo(0);
        RuleFor(user => user.Name).NotNull().NotEmpty().MinimumLength(1).MaximumLength(100).WithMessage("Длина должна быть от 1 до 100");
        RuleFor(user => user.LastName).NotNull().NotEmpty().MinimumLength(1).MaximumLength(100).WithMessage("Длина должна быть от 1 до 100");
        RuleFor(user => user.Age).GreaterThan(0).LessThan(120);
    }
}