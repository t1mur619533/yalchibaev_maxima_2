using AutoMapper;
using DataAccessLayer.Models;

namespace web_api.Models;

public class UserMapConfig : Profile
{
    public UserMapConfig()
    {
        CreateMap<User, UserDto>()
            .ForMember(dto => dto.Id, expression => expression.MapFrom(user => user.Id))
            .ForMember(dto => dto.Age, expression => expression.MapFrom(user => user.Age))
            .ForMember(dto => dto.Name, expression => expression.MapFrom(user => user.FirstName))
            .ForMember(dto => dto.LastName, expression => expression.MapFrom(user => user.LastName))
            .ForMember(dto => dto.PhoneNumber, expression => expression.MapFrom(user => user.PhoneNumber))
            .ForMember(dto => dto.Role, expression => expression.MapFrom(user => (int) user.Role));

        CreateMap<UserDto, User>()
            .ForMember(user => user.Id, expression => expression.MapFrom(dto => dto.Id))
            .ForMember(user => user.Age, expression => expression.MapFrom(dto => dto.Age))
            .ForMember(user => user.FirstName, expression => expression.MapFrom(dto => dto.Name))
            .ForMember(user => user.LastName, expression => expression.MapFrom(dto => dto.LastName))
            .ForMember(user => user.PhoneNumber, expression => expression.MapFrom(dto => dto.PhoneNumber))
            .ForMember(user => user.Role, expression => expression.MapFrom(dto => dto.Role));
        
        CreateMap<CreateUserDto, User>()
            .ForMember(user => user.Id, expression => expression.MapFrom(dto => dto.Id))
            .ForMember(user => user.Login, expression => expression.MapFrom(dto => dto.Login))
            .ForMember(user => user.Password, expression => expression.MapFrom(dto => dto.Password))
            .ForMember(user => user.Age, expression => expression.MapFrom(dto => dto.Age))
            .ForMember(user => user.FirstName, expression => expression.MapFrom(dto => dto.Name))
            .ForMember(user => user.LastName, expression => expression.MapFrom(dto => dto.LastName))
            .ForMember(user => user.PhoneNumber, expression => expression.MapFrom(dto => dto.PhoneNumber))
            .ForMember(user => user.Role, expression => expression.MapFrom(dto => dto.Role));
    }
}