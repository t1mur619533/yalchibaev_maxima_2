using System.Collections.Generic;
using DataAccessLayer.Models;

namespace web_api.Models;

public class UserDto
{
    public int Id { get; set; }
    public string Login { get; set; }
    public string Name { get; set; }
    public string LastName { get; set; }
    public int Age { get; set; }
    public int Role { get; set; }
    
    public string PhoneNumber { get; set; }

    List<Ticket> Tickets { get; set; }
}