using web_api.Models;

namespace web_api.Services;

public interface IUserService : IEntityService<UserDto>
{
}
