using System.Collections.Generic;
using System.Threading.Tasks;

namespace web_api.Services;

public interface IEntityService<T>
{
    T? Get(int userId);
    Task<IEnumerable<T>> Get();
    void Create(T userDto);
    void Remove(int id);
    void Update(T userDto);
}