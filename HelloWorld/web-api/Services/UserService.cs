using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using web_api.Models;

namespace web_api.Services;

public class UserService : IUserService
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    public UserService(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    public UserDto Get(int userId)
    {
        var user = _dataContext.Users
            .AsNoTracking()
            .FirstOrDefault(user1 => user1.Id == userId);
        
        if (user != null)
        {
            return _mapper.Map<UserDto>(user);
        }

        return null;
    }

    public async Task<IEnumerable<UserDto>> Get()
    {
        var userDto = await _dataContext.Users
            .AsNoTracking()// выкл отслеживание изменений
            .Include(user1 => user1.Tickets) //жадной загрузкой получим все Ticket, связанные с пользователем
            .Select(user => _mapper.Map<UserDto>(user))
            .ToListAsync();

        return userDto;
    }

    public void Create(UserDto userDto)
    {
        var user = _mapper.Map<User>(userDto);
        _dataContext.Users.Add(user);
        _dataContext.SaveChanges();
    }

    public void Remove(int userId)
    {
        throw new System.NotImplementedException();
    }

    public void Update(UserDto userDto)
    {
        var user = _dataContext.Users.FirstOrDefault(user1 => user1.Id == userDto.Id);
        if (user == null) return;
        user.Age = userDto.Age;
        user.FirstName = userDto.Name;
        user.LastName = userDto.LastName;
        _dataContext.SaveChanges();
    }
}