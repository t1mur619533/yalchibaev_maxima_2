namespace web_api.Middlewares;

public interface IApiKeyProvider
{
    string GetKey();
}