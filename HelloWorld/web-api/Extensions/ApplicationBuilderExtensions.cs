using Microsoft.AspNetCore.Builder;
using web_api.Middlewares;

namespace web_api.Extensions;

public static class ApplicationBuilderExtensions
{
    public static void UseCustomExceptionHandler(this IApplicationBuilder applicationBuilder)
    {
        applicationBuilder.UseMiddleware<ExceptionsHandlerCustomMiddleware>();
    }
    
    public static void UseApiKeyMiddleware(this IApplicationBuilder applicationBuilder)
    {
        applicationBuilder.UseMiddleware<ApiKeyMiddleware>();
    }
}