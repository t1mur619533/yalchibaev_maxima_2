namespace web_api;

public class Crib
{
    // public string CreateToken(string username, string role)
    // {
    //     var tokenHandler = new JwtSecurityTokenHandler();
    //     var keyval = configuration["Secret"];
    //     var key = Encoding.ASCII.GetBytes(keyval);
    //     var tokenDescriptor = new SecurityTokenDescriptor
    //     {
    //         Subject = new ClaimsIdentity(new[]
    //         {
    //             new Claim(ClaimsIdentity.DefaultNameClaimType, username),
    //             new Claim(ClaimsIdentity.DefaultRoleClaimType, role)
    //         }),
    //         SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
    //             SecurityAlgorithms.HmacSha256Signature),
    //
    //     };
    //     var token = tokenHandler.CreateToken(tokenDescriptor);
    //
    //     var res = tokenHandler.WriteToken(token);
    //     return res;
    // }
    
    // services.AddAuthentication(x =>
    // {
    //     x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    //     x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    //     x.RequireAuthenticatedSignIn = false;
    // })
    // .AddJwtBearer(x =>
    // {
    //     x.RequireHttpsMetadata = false;
    //     x.SaveToken = true;
    //     x.TokenValidationParameters = new TokenValidationParameters
    //     {
    //         ValidateIssuerSigningKey = true,
    //         IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration["Secret"])),
    //         ValidateIssuer = false,
    //         ValidateAudience = false
    //     };
    // });
    
    // services.AddSwaggerGen(c =>
    // {
    //     c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    //     {
    //         In = ParameterLocation.Header,
    //         Description = "Please insert JWT with Bearer into field",
    //         Name = "Authorization",
    //         Type = SecuritySchemeType.ApiKey,
    //         BearerFormat = "JWT"
    //     });
    //
    //     c.AddSecurityRequirement(new OpenApiSecurityRequirement()
    //     {
    //         {
    //             new OpenApiSecurityScheme
    //             {
    //                 Reference = new OpenApiReference {Type = ReferenceType.SecurityScheme, Id = "Bearer"}
    //             },
    //             new string[] { }
    //         }
    //     });
    //
    //     c.SwaggerDoc("v1", new OpenApiInfo
    //     {
    //         Version = "v1",
    //         Title = "Print Center API",
    //         Description = "Print Center Web API",
    //     });
    //     c.CustomSchemaIds(y => y.FullName);
    //
    //     // Set the comments path for the Swagger JSON and UI.
    //     var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    //     var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    //     c.IncludeXmlComments(xmlPath);
    // });
}