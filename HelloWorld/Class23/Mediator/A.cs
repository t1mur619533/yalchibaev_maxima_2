public class A
{
    private Mediator _mediator;

    public A(Mediator mediator)
    {
        _mediator = mediator;
    }

    public void Do()
    {
        _mediator.DoC();
        _mediator.DoB();
    }
}