public class B
{
    private Mediator _mediator;

    public B(Mediator mediator)
    {
        _mediator = mediator;
    }
    
    public void Do()
    {
    }
}