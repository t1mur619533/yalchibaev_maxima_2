public class Mediator
{
    private A _a;
    private B _b;
    private C _c;
    
    public Mediator()
    {
        _a = new A(this);
        _b = new B(this);
        _c = new C(this);
    }

    public void DoA()
    {
        _a.Do();
    }

    public void DoB()
    {
        _b.Do();
    }

    public void DoC()
    {
        _c.Do();
    }
}