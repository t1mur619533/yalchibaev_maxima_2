public interface IReportCreator
{
    Pdf Create();
}