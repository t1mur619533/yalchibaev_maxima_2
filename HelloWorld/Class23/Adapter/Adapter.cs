public class Adapter : IReportCreator //adaptor
{
    private DocReportCreator _docReportCreator;
    
    public Adapter(DocReportCreator docReportCreator)
    {
        _docReportCreator = docReportCreator;
    }
    
    public Pdf Create()
    {
        Doc docReport = _docReportCreator.Create();
        //doc to pdf
        return new Pdf();
    }
}