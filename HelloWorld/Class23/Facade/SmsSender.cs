public class SmsSender : ISmsSender
{
    private IAuth _auth;
    private IProvider _provider;
    
    public void Send(string num)
    {
        _auth.Auth();
        _provider.SetToken();
        Console.WriteLine("Смс отправлен");
    }
}