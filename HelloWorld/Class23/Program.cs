﻿Console.WriteLine();

public interface ICommand
{
    void Execute();
    void Cancel();
}

public class SaveCommand : ICommand
{
    public void Execute()
    {
        Console.WriteLine("Сохранение на диск");
    }

    public void Cancel()
    {
        Console.WriteLine("Отмена сохранения на диск");
    }
}