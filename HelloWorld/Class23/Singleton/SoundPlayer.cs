public sealed class SoundPlayer
{
    private static SoundPlayer _instance;
    
    private SoundPlayer()
    {
    }

    public static SoundPlayer Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new SoundPlayer();
            }

            return _instance;
        }
    }

    public void Play(string soundPath)
    {
        Console.WriteLine($"Воспоизводим звук {soundPath}");
    }
}