public abstract class Logistics : ILogistics
{
    public abstract ITransport CreateTransport();

    public void Delivery(string item)
    {
        var transport = CreateTransport();
        transport.Delivery(item);
    }
}