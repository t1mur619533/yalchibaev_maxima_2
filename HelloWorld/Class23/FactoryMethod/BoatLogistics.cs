public class BoatLogistics : Logistics
{
    public override ITransport CreateTransport()
    {
        return new Boat();
    }
}