public interface ILogistics
{
    ITransport CreateTransport();

    void Delivery(string item);
}