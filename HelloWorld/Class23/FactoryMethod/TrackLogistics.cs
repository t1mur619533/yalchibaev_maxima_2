public class TrackLogistics : Logistics
{
    public override ITransport CreateTransport()
    {
        return new Track();
    }
}