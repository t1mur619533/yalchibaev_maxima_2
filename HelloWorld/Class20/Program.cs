﻿List<Task> tasks = new List<Task>();

var cancellationTokenSource = new CancellationTokenSource();
var task = TaskOperation(cancellationTokenSource);
Task.Delay(3000).Wait();
cancellationTokenSource.Cancel();
Task.Delay(500).Wait();
Console.WriteLine($"Задача отменена через 3 секунды {task.Status}");

Task TaskOperation(CancellationTokenSource cts)
{
    return Task.Run(() =>
    {
        for (int i = 0; i < 1000; i++)
        {
            if (cts.IsCancellationRequested)
            {
                cts.Token.ThrowIfCancellationRequested();
            }

            Task.Delay(500).Wait();
            Console.WriteLine($"{i} iteration");
        }
    }, cts.Token);
}