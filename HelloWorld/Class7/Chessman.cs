namespace Class7;

public class Chessman
{
    protected int X;
    protected int Y;
    private Color Color;

    public int X_property
    {
        get
        {
            return X;
        }
        set
        {
            X = value;
        }
    }
    public int Y_property
    {
        get
        {
            return Y;
        }
    }

    public Chessman(int x, int y)
    {
        X = x;
        Y = y;
    }
    
    public Chessman(Color color)
    {
        Color = color;
    }

    public Chessman(int x, int y, Color color)
    {
        X = x;
        Y = y;
        Color = color;
    }

    public virtual void Move(int x, int y)
    {
        if(x >= 1 && x <=8)
            X = x;
        
        if(y >= 1 && y <=8)
            Y = y;
    }
}