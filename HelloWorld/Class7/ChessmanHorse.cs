namespace Class7;

public class ChessmanHorse : Chessman
{
    public ChessmanHorse(int x, int y) : base(x, y)
    {
    }

    public ChessmanHorse(Color color) : base(color)
    {
    }

    public ChessmanHorse(int x, int y, Color color) : base(x, y, color)
    {
    }

    public override void Move(int x, int y)
    {
        X = x;
        Y = y;
        Console.WriteLine($"Сходил на {X}:{Y}");
    }
}