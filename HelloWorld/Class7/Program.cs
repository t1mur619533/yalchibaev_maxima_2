﻿// See https://aka.ms/new-console-template for more information

using Class7;

Chessman horseWhite = new Chessman(5, 6);
horseWhite.Move(8,8);
horseWhite.Move(-9,8);

Console.WriteLine($"Координата: {horseWhite.X_property}:{horseWhite.Y_property}");

Chessman horseBlack = new Chessman(Color.Black);
horseBlack.Move(4,5);

Car car = new Car();
car.Model = Model.Audi;

Car car2 = new Car();
car.Model = Model.Mersedes;

Car car3 = new Car();
car.Model = Model.BMW;