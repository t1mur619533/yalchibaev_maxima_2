﻿using Microsoft.Extensions.DependencyInjection;

var serviceCollection = new ServiceCollection();
serviceCollection.AddTransient<ILogger, Logger>(); // регистрация зависимости
serviceCollection.AddSingleton<IHelloService, HelloService>(); // регистрация зависимости
serviceCollection.AddTransient<IWriter, RedWriter>(); // регистрация зависимости

var serviceProvider = serviceCollection.BuildServiceProvider(); // далее не можем регистрировать зависимости в serviceCollection

for (int i = 0; i < 5; i++)
{
    var helloService = serviceProvider.GetService<IHelloService>();
    helloService.SayHello();
}