public class Logger : ILogger
{
    private IWriter _writer;

    public Logger(IWriter writer)
    {
        _writer = writer;
    }

    public void Log(string message)
    {
        _writer.Write(message);
    }
}