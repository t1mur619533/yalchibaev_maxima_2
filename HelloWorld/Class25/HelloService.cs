public class HelloService : IHelloService
{
    private ILogger _logger;
    private int _seed;

    public HelloService(ILogger logger)
    {
        _logger = logger;
        _seed = new Random().Next(0, 1000);
    }

    public void SayHello()
    {
        _logger.Log($"Hello {_seed}!");
    }
}