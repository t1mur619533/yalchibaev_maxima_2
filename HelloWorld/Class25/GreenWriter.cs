public class GreenWriter : IWriter
{
    public void Write(string message)
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine(message);
    }
}