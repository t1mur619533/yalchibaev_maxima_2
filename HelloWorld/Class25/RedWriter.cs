public class RedWriter : IWriter
{
    public void Write(string message)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine(message);
    }
}