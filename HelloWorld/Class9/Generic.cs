namespace Class9;

public class MyList<T> where T : struct
{
    private T[] array = new T[10];
    private int i = 0;
    
    public void Add(T item)
    {
        if (i > array.Length - 1)
            i = 0;
        
        array[i] = item;
        i++;
    }

    public void Print()
    {
        foreach (var item in array)
        {
            Console.WriteLine(item);
        }
    }
}

public class MyTypePrinter
{
    public void Print<T>(T type)
    {
        Console.WriteLine($"my type : {type.GetType()}");
    }
}