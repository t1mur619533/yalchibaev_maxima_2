﻿using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

var optionsBuilder = new DbContextOptionsBuilder<DataContext>()
   .UseNpgsql("Host=localhost;Port=5432;Database=Users;Username=postgres;Password=postgres");
var dataContext = new DataContext(optionsBuilder.Options);
dataContext.Users.Add(new User()
{
   FirstName = "Ivan",
   LastName = "Sidorov",
   Age = 19,
   Role = Role.Admin,
   PhoneNumber = "9(999)355-34-23"
});
dataContext.SaveChanges();

Console.ReadKey();