using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Models;

[Table("Customers")]
public class User
{
    public int Id { get; set; }

    public string Login { get; set; }
    
    public string Password { get; set; }

    [Required]
    [MaxLength(30)]
    public string FirstName { get; set; }
    
    [Required]
    [MaxLength(30)]
    public string LastName { get; set; }

    public int Age { get; set; }
    
    public string PhoneNumber { get; set; }
    
    public Role Role { get; set; }
    
    public virtual ICollection<Ticket> Tickets { get; set; } //навигационное свойство
}