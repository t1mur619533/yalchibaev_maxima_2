namespace DataAccessLayer.Models;

public class Ticket
{
    public int Id { get; set; }
    public string Name { get; set; }
    public DateTime Date { get; set; }

    public int UserId { get; set; } //внешний ключ
    public virtual User User { get; set; } //навигационное свойство
}