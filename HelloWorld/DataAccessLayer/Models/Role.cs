namespace DataAccessLayer.Models;

public enum Role : int
{
    Disable,
    Guest,
    User,
    Admin
}