using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DataAccessLayer;

public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
{
    public DataContext CreateDbContext(string[] args)
    {
        var optionsBuilder = new DbContextOptionsBuilder<DataContext>()
            .UseNpgsql("Host=localhost;Port=5432;Database=Users;Username=postgres;Password=postgres");
        return new DataContext(optionsBuilder.Options);
    }
}