﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccessLayer.Migrations
{
    public partial class AddNewTicketsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CinemaTickets_Customers_UserId",
                table: "CinemaTickets");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CinemaTickets",
                table: "CinemaTickets");

            migrationBuilder.DropColumn(
                name: "Desctiption",
                table: "CinemaTickets");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "CinemaTickets");

            migrationBuilder.RenameTable(
                name: "CinemaTickets",
                newName: "Tickets");

            migrationBuilder.RenameIndex(
                name: "IX_CinemaTickets_UserId",
                table: "Tickets",
                newName: "IX_Tickets_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tickets",
                table: "Tickets",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "NewTickets",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false),
                    Desctiption = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewTickets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NewTickets_Tickets_Id",
                        column: x => x.Id,
                        principalTable: "Tickets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Tickets_Customers_UserId",
                table: "Tickets",
                column: "UserId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tickets_Customers_UserId",
                table: "Tickets");

            migrationBuilder.DropTable(
                name: "NewTickets");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tickets",
                table: "Tickets");

            migrationBuilder.RenameTable(
                name: "Tickets",
                newName: "CinemaTickets");

            migrationBuilder.RenameIndex(
                name: "IX_Tickets_UserId",
                table: "CinemaTickets",
                newName: "IX_CinemaTickets_UserId");

            migrationBuilder.AddColumn<string>(
                name: "Desctiption",
                table: "CinemaTickets",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "CinemaTickets",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CinemaTickets",
                table: "CinemaTickets",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CinemaTickets_Customers_UserId",
                table: "CinemaTickets",
                column: "UserId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
