using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations;

public class TicketConfiguration : IEntityTypeConfiguration<Ticket>
{
    public void Configure(EntityTypeBuilder<Ticket> modelBuilder)
    {
        modelBuilder.HasKey(ticket => ticket.Id);
        modelBuilder
            .HasOne<User>(ticket => ticket.User) //один ко многим
            .WithMany(user => user.Tickets) // у одного пользователя может быть много билетов
            .OnDelete(DeleteBehavior.Cascade);
        modelBuilder.ToTable("Tickets");
        modelBuilder.Property("Name").HasMaxLength(100);
    }
}