﻿// See https://aka.ms/new-console-template for more information

using (var reader = new FileReader())
{
    reader.ReadByte("");
}

var reader2 = new FileReader();
try
{
    reader2.ReadByte("");
}
finally
{
    reader2.Dispose();
}

int DoSmth()
{
    int i = 100;
    int m = 60;
    var user = new User("Timur");
    return i * m;
}

class User
{
    public string Name { get; set; }

    public User(string name)
    {
        Name = name;
        //connect to database
    }

    ~User()
    {
        Name = null;
    }
}

class FileReader : IDisposable
{
    private bool disposed;
    
    public byte[] ReadByte(string path)
    {
        Console.WriteLine("ReadByte");
        return null;
    }

    public void Dispose()
    {
        Console.WriteLine("Dispose");
        disposed = true;
        GC.SuppressFinalize(this);
    }
    
    ~FileReader()
    {
        if (!disposed)
        {
            Dispose();
        }
    }
}