﻿using Class12;

CriticalState r = OnBatteryDischarged;
r += OnBatteryDischarged;

r.Invoke("adfdsf");

var battery = new Battery();
battery.OnCriticalBatteryState += OnBatteryDischarged;
battery.OnBatteryEmpty += OnBatteryEmpty;



for (int i = 0; i < 10; i++)
{
    battery.Discharge();
}

battery.OnCriticalBatteryState -= OnBatteryDischarged;

void OnBatteryDischarged(string message)
{
    Console.WriteLine(message);
}

void OnBatteryEmpty()
{
    Console.WriteLine("Батарея пуста");
}