namespace Class12;

public class Battery
{
    private int _state;
    
    public event CriticalState OnCriticalBatteryState;
    public event Action OnBatteryEmpty;

    public Battery()
    {
        _state = 100;
    }

    public void Charge()
    {
        _state += 10;
    }
    
    public void Discharge()
    {
        if (_state <= 0)
        {
            OnBatteryEmpty.Invoke();
            return;
        }

        _state -= 10;
        if (_state < 30)
        {
            if (OnCriticalBatteryState != null)
            {
                OnCriticalBatteryState.Invoke($@"Критический заряд батареи {_state}");
            }
        }
    }
}

public delegate void CriticalState(string message);