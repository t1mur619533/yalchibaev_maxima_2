using DataAccessLayer;
using Domain.Security;
using MediatR;

namespace Domain.Accounts.Queries;

public class LoginQueryHandler : IRequestHandler<LoginQuery, string>
{
    private readonly DataContext _dataContext;
    private readonly IJWTProvider _jwtTokenProvider;

    public LoginQueryHandler(DataContext dataContext, IJWTProvider jwtTokenProvider)
    {
        _dataContext = dataContext;
        _jwtTokenProvider = jwtTokenProvider;
    }

    public Task<string> Handle(LoginQuery request, CancellationToken cancellationToken)
    {
        var user = _dataContext.Users.First(user1 => user1.Login == request.Login && user1.Password == request.Password);
        return Task.FromResult(_jwtTokenProvider.GetToken(user.Login, user.Role.ToString(), user.Id));
    }
}