using MediatR;

namespace Domain.Accounts.Queries;

public class LoginQuery : IRequest<string>
{
    public string Login { get; set; }
    public string Password { get; set; }
}