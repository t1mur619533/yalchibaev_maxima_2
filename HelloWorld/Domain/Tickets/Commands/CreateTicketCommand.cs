using MediatR;

namespace Domain.Tickets.Commands;

public class CreateTicketCommand : IRequest<int>
{
    public int Id { get; set; }
    
    public string Name { get; set; }

    public int Userid { get; set; }
}