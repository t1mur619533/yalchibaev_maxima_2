using AutoMapper;
using DataAccessLayer.Models;

namespace Domain.Tickets.Commands;

public class CreateTicketMapProfile : Profile
{
    public CreateTicketMapProfile()
    {
        CreateMap<CreateTicketCommand, Ticket>()
            .ForMember(ticket => ticket.Id, expression => expression.MapFrom(command => command.Id))
            .ForMember(ticket => ticket.Name, expression => expression.MapFrom(command => command.Name))
            .ForMember(ticket => ticket.UserId, expression => expression.MapFrom(command => command.Userid));
    }
}