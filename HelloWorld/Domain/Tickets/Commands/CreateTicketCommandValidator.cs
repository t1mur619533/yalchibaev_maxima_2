using FluentValidation;

namespace Domain.Tickets.Commands;

public class CreateTicketCommandValidator : AbstractValidator<CreateTicketCommand>
{
    public CreateTicketCommandValidator()
    {
        RuleFor(command => command.Id).NotNull().GreaterThanOrEqualTo(0);
        RuleFor(command => command.Name).NotNull().NotEmpty().Length(3, 20);
        RuleFor(command => command.Userid).NotNull().GreaterThanOrEqualTo(0);
    }
}