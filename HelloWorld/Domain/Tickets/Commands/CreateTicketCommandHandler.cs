using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using MediatR;

namespace Domain.Tickets.Commands;

public class CreateTicketCommandHandler : IRequestHandler<CreateTicketCommand, int>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    public CreateTicketCommandHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    public async Task<int> Handle(CreateTicketCommand request, CancellationToken cancellationToken)
    {
        var ticket = _mapper.Map<Ticket>(request);
        ticket.Date = DateTime.Now.ToUniversalTime();
        await _dataContext.Tickets.AddAsync(ticket, cancellationToken);
        await _dataContext.SaveChangesAsync(cancellationToken);
        return ticket.Id;
    }
}