namespace Domain.Tickets;

/// <summary>
/// Билет
/// </summary>
public class TicketDto
{
    /// <summary>
    /// Индентификатор билета
    /// </summary>
    public int Id { get; set; }
    
    /// <summary>
    /// Наименование билета
    /// </summary>
    public string Name { get; set; }
    
    /// <summary>
    /// Индентификатор владельца билета
    /// </summary>
    public int UserId { get; set; }
}