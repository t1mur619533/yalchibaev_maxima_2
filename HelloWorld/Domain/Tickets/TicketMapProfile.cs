using AutoMapper;
using DataAccessLayer.Models;

namespace Domain.Tickets;

public class TicketMapProfile : Profile
{
    public TicketMapProfile()
    {
        CreateMap<Ticket, TicketDto>()
            .ForMember(dto => dto.Id, expression => expression.MapFrom(ticket => ticket.Id))
            .ForMember(dto => dto.Name, expression => expression.MapFrom(ticket => ticket.Name))
            .ForMember(dto => dto.UserId, expression => expression.MapFrom(ticket => ticket.UserId));
            
        CreateMap<TicketDto, Ticket>()
            .ForMember(ticket => ticket.Id, expression => expression.MapFrom(dto => dto.Id))
            .ForMember(ticket => ticket.Name, expression => expression.MapFrom(dto => dto.Name))
            .ForMember(ticket => ticket.UserId, expression => expression.MapFrom(dto => dto.UserId));
    }
}