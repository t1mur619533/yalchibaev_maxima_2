using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Tickets.Queries;

public class GetTicketsQueryHandler : IRequestHandler<GetTicketsQuery, IEnumerable<TicketDto>>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    public GetTicketsQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    public async Task<IEnumerable<TicketDto>> Handle(GetTicketsQuery request, CancellationToken cancellationToken)
    {
        var tickets = await _dataContext.Tickets
            .Skip(request.Skip)
            .Take(request.Take)
            .AsNoTracking() //выключаем отслеживание для более быстрого выполнения запроса
            .ToListAsync(cancellationToken: cancellationToken);
        
        return _mapper.Map<IEnumerable<TicketDto>>(tickets);
    }
}