using MediatR;

namespace Domain.Tickets.Queries;

public class GetTicketQuery : IRequest<TicketDto>
{
    public int Id { get; set; }
}