using AutoMapper;
using DataAccessLayer;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Tickets.Queries;

public class GetTicketQueryHandler : IRequestHandler<GetTicketQuery, TicketDto>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    public GetTicketQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    public async Task<TicketDto> Handle(GetTicketQuery request, CancellationToken cancellationToken)
    {
        var ticket = await _dataContext.Tickets.AsNoTracking().FirstOrDefaultAsync(ticket1 => ticket1.Id == request.Id, cancellationToken: cancellationToken);
        if (ticket == null)
            throw new NotFoundException($"Билет с id={request.Id} не найден");
        
        return _mapper.Map<TicketDto>(ticket);
    }
}