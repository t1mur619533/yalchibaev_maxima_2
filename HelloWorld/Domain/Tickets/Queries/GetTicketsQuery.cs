using MediatR;

namespace Domain.Tickets.Queries;

public class GetTicketsQuery : IRequest<IEnumerable<TicketDto>>
{
    public int Skip { get; set; }
    public int Take { get; set; }
}