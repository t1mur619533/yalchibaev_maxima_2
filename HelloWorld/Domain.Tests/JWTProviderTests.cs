using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Domain.Security;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using NSubstitute;

namespace Domain.Tests;

public class JWTProviderTests
{
    [Fact]
    public void Create_JWT_ExpectSuccess()
    {
        var domainOptions = new DomainOptions
        {
            Secret = "secret-key-secret-key-secret-key-secret-key"
        };
        var options = Substitute.For<IOptions<DomainOptions>>();
        options.Value.Returns(domainOptions);
        var jwtTokenProvider = new JWTProvider(options);

        var login = "testLogin";
        var role = "testRole";
        var id = 1;

        var token = jwtTokenProvider.GetToken(login, role, id);
        
        var claimsPrincipal = new JwtSecurityTokenHandler().ValidateToken(token, new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(domainOptions.Secret)),
            ValidateIssuer = false,
            ValidateAudience = false
        }, out var securityToken );

        Assert.Contains(claimsPrincipal.Claims, claim => claim.Value == login);
        Assert.Contains(claimsPrincipal.Claims, claim => claim.Value == role);
        Assert.Contains(claimsPrincipal.Claims, claim => claim.Value == id.ToString());
    }
    
    [Fact]
    public void Create_JWT_with_short_key_ExpectThrowsArgumentOutOfRangeException()
    {
        var domainOptions = new DomainOptions
        {
            Secret = "secret"
        };
        var options = Substitute.For<IOptions<DomainOptions>>();
        options.Value.Returns(domainOptions);
        var jwtTokenProvider = new JWTProvider(options);

        var login = "testLogin";
        var role = "testRole";
        var id = 1;

        Assert.Throws<ArgumentOutOfRangeException>(() => { jwtTokenProvider.GetToken(login, role, id); });
    }
}