namespace Domain.Tests;

public class Calc
{
    public int Sum(int a, int b)
    {
        var absA = Math.Abs(a);
        var absB = Math.Abs(b);
        
        return absA + absB;
    }
}