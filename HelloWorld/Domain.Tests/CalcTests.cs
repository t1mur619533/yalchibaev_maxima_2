using FluentAssertions;

namespace Domain.Tests;

public class CalcTests
{
    [Theory]
    [InlineData(9,3)]
    [InlineData(3,234)]
    [InlineData(-9,0)]
    [InlineData(9,4356)]
    public void Sum(int a, int b)
    {
        var expected = a + b;

        var calc = new Calc();
        var actual = calc.Sum(a, b);

        // asserts
        actual.Should().Be(expected);
    }
}