﻿Console.WriteLine();

public interface ISocket
{
    void Open();
    void Bind();
    void Send(byte[] bytes);
    Task SendAsync(byte[] bytes);
}

public class NotificationSender
{
    protected ISocket _socket;

    public NotificationSender(ISocket socket)
    {
        _socket = socket;
    }

    public virtual void Send(string msg)
    {
        _socket.Send(null);
    }
}

public class OtherNotificationSender : NotificationSender
{
    public OtherNotificationSender(ISocket socket) : base(socket)
    {
    }

    public override async void Send(string msg)
    {
        byte[] bytes = new byte[] { };
        await _socket.SendAsync(bytes);
    }
}