namespace Class13;

public class ValidateAgeAttribute : Attribute
{
    public int Max { get; }

    public ValidateAgeAttribute(int max)
    {
        Max = max;
    }
}