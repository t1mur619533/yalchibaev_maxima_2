using System.Runtime.CompilerServices;

namespace Class13;

public partial class User
{
    public partial void Print()
    {
        Console.WriteLine($"{Name} : {Age}");
    }
}