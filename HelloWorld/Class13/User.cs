namespace Class13;

[ValidateAge(100)]
public partial class User
{
    public string Name { get; set; }
    public int Age { get; set; }

    public partial void Print();
}