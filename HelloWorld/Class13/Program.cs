﻿using Class13;
using Class13_Extensions;

var user = new User();
user.Print();

int[] array = new int[10];
array.Randomize();
array.Reset();
array.Fill(6);

var max = array.Max();
var sum = array.Sum();

string[] strArray = new string[10];
strArray.Reset();

for (int i = 0; i < array.Length; i++)
{
    Console.WriteLine(array[i]);
}


// User user = new User(){Age = 100};
// var student = new { Name = "Timur", University = "University" };
//
// Console.WriteLine($"{student.Name}:{student.University}");