﻿Console.WriteLine($"Готовим завтрак! Поток: {Thread.CurrentThread.ManagedThreadId}");

try
{
    await PourCoffee();
    await HeatPan();
    await FryEggs();
    await FryBacon();
    await ToastBread();
    await JamOnBread();
    string juiceName = await PourJuice();

    Console.WriteLine($"Завтрак готов! Поток: {Thread.CurrentThread.ManagedThreadId}");
}
catch (Exception e)
{
    Console.WriteLine(e);
    throw;
}

async Task PourCoffee()
{
    Console.WriteLine($"Наливаем кофе Поток: {Thread.CurrentThread.ManagedThreadId}");
    await Task.Delay(1000);
}

async Task HeatPan()
{
    Console.WriteLine($"Греем сковородку Поток: {Thread.CurrentThread.ManagedThreadId}");
    await Task.Delay(1000);
}

async Task FryEggs()
{
    Console.WriteLine("Жарим яйца");
    await Task.Delay(1000);
}

async Task FryBacon()
{
    Console.WriteLine("Жарим бекон");
    await Task.Delay(2000);
}

async Task ToastBread()
{
    Console.WriteLine("Готовим тосты");
    await Task.Delay(1000);
}

async Task JamOnBread()
{
    Console.WriteLine("Мажем джем на тосты");
    await Task.Delay(1000);
}

async Task<string> PourJuice()
{
    Console.WriteLine("Наливаем сок");
    await Task.Delay(1000);
    return "Rich";
}
