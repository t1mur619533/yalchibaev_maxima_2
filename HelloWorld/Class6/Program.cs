﻿// See https://aka.ms/new-console-template for more information
using System.Collections;

int[] masA = new int[3];
int[] masB = new int[3];
masB[0] = 4;
masB[1] = 4;
masB[2] = 6;

var r = Sum(masB);

Color myColor;
myColor = Color.Yellow;

Console.WriteLine($"Белый цвет: {(int) myColor}");

int[] masC = new int[3];

void Print(bool vertical)
{
    if (vertical)
    {
        //выводим массив в консоль вертикально
    }
    else
    {
        //горизонтально
    }
}

int Sum(params int[] array)
{
    var s = 0;
    for (int i = 0; i < array.Length; i++)
    {
        s = s + array[i];
    }

    return s;
}

public enum Color
{
    White = 100,
    Yellow,
    Black = 900,
    Red,
    Green
}