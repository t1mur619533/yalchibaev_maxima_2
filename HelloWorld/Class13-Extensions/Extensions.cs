namespace Class13_Extensions;

public static class Extensions
{
    public static void Reset<T>(this T[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            array[i] = default;
        }
    }
    
    public static T[] Clone<T>(this T[] array)
    {
        var temp = new T[array.Length];

        for (int i = 0; i < array.Length; i++)
        {
            temp[i] = array[i];
        }

        return temp;
    }
    
    public static void Randomize(this int[] array)
    {
        var rand = new Random();

        for (int i = 0; i < array.Length; i++)
        {
            array[i] = rand.Next(0, 10);
        }
    }
    
    public static void Fill(this int[] array, int a)
    {
        for (int i = 0; i < array.Length; i++)
        {
            array[i] = a;
        }
    }
}