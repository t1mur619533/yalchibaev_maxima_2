﻿// See https://aka.ms/new-console-template for more information

int[][] array = new int[4][];

array[0] = new int[4];
array[1] = new int[5];
array[2] = new int[3];
array[3] = new int[6];

for (int a = 0; a < array.Length; a++)
{
    for (int b = 0; b < array[a].Length; b++)
    {
        array[a][b] = new Random().Next(50, 99);
        Console.Write(array[a][b] + " ");
    }
    Console.WriteLine();
}

Console.WriteLine();