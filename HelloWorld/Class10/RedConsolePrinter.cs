namespace Class10;

public class RedConsolePrinter : Printer
{
    public override void Print(string text)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine(text);
    }
}