namespace Class10;

public class PrinterProvider
{
    private Printer _printer;

    public PrinterProvider(Printer printer)
    {
        _printer = printer;
    }

    public void SetPrinter(Printer printer)
    {
        _printer = printer;
    }
    
    public void ConsoleWriteLine(string text)
    {
        _printer.Print(_printer.GetPrinterName());
        _printer.Print(text);
    }
}