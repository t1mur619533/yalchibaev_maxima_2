namespace Class10;

public class GreenConsolePrinter : Printer
{
    public override void Print(string text)
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine(text);
    }
}