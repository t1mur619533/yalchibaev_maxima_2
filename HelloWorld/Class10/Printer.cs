namespace Class10;

public abstract class Printer
{
    public abstract void Print(string text);
    
    public string GetPrinterName()
    {
        return GetType().Name;
    }
}