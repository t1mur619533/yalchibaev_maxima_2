﻿// See https://aka.ms/new-console-template for more information

using Class10;

var printerProvider = new PrinterProvider(new RedConsolePrinter());
Console.WriteLine("Выберите цвет");
Console.WriteLine("1 = Красный");
Console.WriteLine("2 = Зеленый");
Console.WriteLine("3 = Синий");
var num = Convert.ToInt32(Console.ReadLine());
switch (num)
{
    case 1:
        var printer1 = new RedConsolePrinter();
        printerProvider.SetPrinter(printer1);
        break;
    case 2:
        printerProvider.SetPrinter(new GreenConsolePrinter());
        break;
    case 3:
        printerProvider.SetPrinter(new BlueConsolePrinter());
        break;
}

printerProvider.ConsoleWriteLine($"Вы выбрали № {num}");