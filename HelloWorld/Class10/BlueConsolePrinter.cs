namespace Class10;

public class BlueConsolePrinter : Printer
{
    public override void Print(string text)
    {
        Console.ForegroundColor = ConsoleColor.Blue;
        Console.WriteLine(text);
    }
}