﻿using System.Collections;

List<int> list = new List<int>(10);

for (int i = 0; i < list.Count; i++)
{
    list.Add(i);
}

foreach (var l in list)
{
    Console.WriteLine(l);
}


Console.WriteLine($"Количество элементов в list: {list.Count}, емкость: {list.Capacity}");

list.Clear();
Console.WriteLine($"Количество элементов в list: {list.Count}, емкость: {list.Capacity}");

Queue<int> queue = new Queue<int>(9);

for (int i = 0; i < 5; i++)
{
    queue.Enqueue(i);
}

foreach (var q in queue)
{
    Console.WriteLine($"- {q}");
}

Console.WriteLine($"Элементов в очереди {queue.Count}");
Console.WriteLine($"Первый элемент {queue.Dequeue()}");
Console.WriteLine($"Второй элемент {queue.Peek()}");
Console.WriteLine($"Элементов в очереди {queue.Count}");

Stack<int> stack = new Stack<int>(10);

for (int i = 0; i < 5; i++)
{
    stack.Push(i);
}

foreach (var s in stack)
{
    Console.WriteLine(s);
}

Console.WriteLine($"Элементов в стеке {stack.Count}");
Console.WriteLine($"Первый элемент {stack.Peek()}");
Console.WriteLine($"Первый элемент {stack.Pop()}");
Console.WriteLine($"Элементов в стеке {stack.Count}");

Dictionary<string, string> dictionary = new Dictionary<string, string>();

dictionary.Add("key1", "value");
dictionary.Add("key2", "value");
dictionary.Add("key3", "value");
dictionary.Add("key4", "value");

foreach (var kv in dictionary)
{
    Console.WriteLine($"{kv.Key} : {kv.Value}");
}

Console.WriteLine($"элемент с ключом key1 {dictionary["key1"]}");
