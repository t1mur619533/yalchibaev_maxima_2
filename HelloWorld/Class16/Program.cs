﻿// See https://aka.ms/new-console-template for more information

using Class16;

List<Group> groups = new List<Group>();

var group1 = new Group();
List<Student> g1Students = new List<Student>();
g1Students.Add(new Student() { Id = 0, Age = 19, FirstName = "Ivan", LastName = "Ivanov" });
g1Students.Add(new Student() { Id = 1, Age = 34, FirstName = "Sergey", LastName = "Sidorov" });
group1.Students = g1Students;
groups.Add(group1);

groups.Add(new Group()
{
    Students = new List<Student>()
    {
        new Student(){Id = 0, Age = 19, FirstName = "Ivan", LastName = "Ivanov"},
        new Student(){Id = 1, Age = 34, FirstName = "Sergey", LastName = "Sidorov"}
    }
});

groups.Add(new Group()
{
    Students = new List<Student>()
    {
        new Student(){Id = 2, Age = 24, FirstName = "Denis", LastName = "Petrov"},
        new Student(){Id = 3, Age = 21, FirstName = "Timur", LastName = "Yalchibaev"},
        new Student(){Id = 4, Age = 16, FirstName = "Airat", LastName = "Ivanov"}
    }
});

List<Student> students = new List<Student>();
students.Add(new Student() { Id = 0, Age = 19, FirstName = "Ivan", LastName = "Ivanov" });
students.Add(new Student() { Id = 1, Age = 19, FirstName = "Alex", LastName = "Alexandrov" });
students.Add(new Student() { Id = 2, Age = 34, FirstName = "Sergey", LastName = "Sidorov" });
students.Add(new Student(){Id = 3, Age = 24, FirstName = "Denis", LastName = "Petrov"});
students.Add(new Student(){Id = 4, Age = 21, FirstName = "Timur", LastName = "Yalchibaev"});
students.Add(new Student(){Id = 5, Age = 16, FirstName = "Airat", LastName = "Ivanov"});

IEnumerable<Student> studentsEnumerable = students;

foreach (var student in studentsEnumerable)
{
    Console.WriteLine($"{student.Id}. {student.Age} : {student.FirstName} {student.LastName}");
}

Console.WriteLine("==================");

Student[] page1 = studentsEnumerable
    .Skip(1)
    .Take(2)
    .ToArray();

Console.WriteLine("1 страница");
foreach (var student in page1)
{
    Console.WriteLine($"{student.Id}. {student.Age} : {student.FirstName} {student.LastName}");
}

List<Student> page2 = studentsEnumerable
    .Skip(2)
    .Take(2)
    .ToList();

Console.WriteLine("2 страница");
foreach (var student in page2)
{
    Console.WriteLine($"{student.Id}. {student.Age} : {student.FirstName} {student.LastName}");
}

Console.WriteLine("==================");

Console.WriteLine("================== SelectMany");
var selectMany = groups.SelectMany(group => group.Students);

foreach (var student in selectMany)
{
    Console.WriteLine($"{student.Id}. {student.Age} : {student.FirstName} {student.LastName}");
}
Console.WriteLine("==================");

Console.WriteLine("================== Reverse");
var reverse = studentsEnumerable.Reverse();

foreach (var student in reverse)
{
    Console.WriteLine($"{student.Id}. {student.Age} : {student.FirstName} {student.LastName}");
}
Console.WriteLine("==================");

Console.WriteLine("================== All");
var all = studentsEnumerable.All(MoreThan21);
Console.WriteLine($"Все студенты старше 21 года : {all}");
var all2 = studentsEnumerable.All(student => student.Age < 100);
Console.WriteLine($"Все студенты моложе 100 лет : {all2}");
Console.WriteLine("==================");

Console.WriteLine("================== Any");
var any = studentsEnumerable.Any(student => student.Age < 18);
Console.WriteLine($"Среди студентов есть несовершеннотелний : {any}");

var any2 = studentsEnumerable.Any(student => student.LastName == "Ivanov");
Console.WriteLine($"Среди студентов есть Ivanov : {any2}");
Console.WriteLine("==================");

Console.WriteLine("================== Contains");
var contains = studentsEnumerable
    .Select(student => student.FirstName)
    .Contains("Timur");
Console.WriteLine($"Среди студентов есть Timur : {contains}");
Console.WriteLine("==================");

Console.WriteLine("================== Count");
var count = studentsEnumerable.Count();
Console.WriteLine($"Всего студентов : {count}");

var count2 = studentsEnumerable.Count(student => student.Age % 2 == 0);
Console.WriteLine($"Студентов с четным возрастом : {count2}");
Console.WriteLine("==================");


Console.WriteLine("================== Sum");
var sum = studentsEnumerable.Select(student => student.Age).Sum();
Console.WriteLine($"Общий возраст студентов : {sum}");
Console.WriteLine("==================");


Console.WriteLine("================== Average");
var average = studentsEnumerable.Select(student => student.Age).Average();
Console.WriteLine($"Средний возраст студентов : {average}");
Console.WriteLine("==================");

Console.WriteLine("================== Min Max");
var min = studentsEnumerable.Select(student => student.Age).Min();
Console.WriteLine($"Минимальный возраст среди студентов : {min}");
var max = studentsEnumerable.Select(student => student.Age).Max();
Console.WriteLine($"Максимальный возраст среди студентов : {max}");
Console.WriteLine("==================");


Console.WriteLine("================== OrderBy");
var orderBy = studentsEnumerable
    .OrderBy(student => student.Age)
    .ThenBy(student => student.FirstName);
foreach (var student in orderBy)
{
    Console.WriteLine($"{student.Id}. {student.Age} : {student.FirstName} {student.LastName}");
}
Console.WriteLine("==================");

Console.WriteLine("===>> 10 случайных чисел");
var stringNumbers = new List<string>();
for (int i = 0; i < 10; i++)
{
    var numberStr = new Random().Next(-100, 100).ToString();
    Console.Write($"{numberStr} ");
    stringNumbers.Add(numberStr);
}
var numbers = ParseStrings(stringNumbers);
var sumChet = numbers
    .Where(n => n > 0)
    .Where(n => n % 2 == 0)
    .Sum();
var sumNechet = numbers
    .Where(n => n > 0)
    .Where(n => n % 2 != 0)
    .Sum();
var result = sumChet - sumNechet;
Console.WriteLine();
Console.WriteLine($"Разница между суммой четных и нечетных чисел, кроме отрицательных: {result}");

Console.WriteLine("==================");

int[] ParseStrings(IEnumerable<string> strings)
{
    return strings.Select(s => int.Parse(s)).ToArray();
}

bool MoreThan21(Student student)
{
    if (student.Age > 21)
        return true;
    else
    {
        return false;
    }
}
